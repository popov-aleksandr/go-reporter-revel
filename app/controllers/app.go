package controllers

import (
	"github.com/revel/revel"
	"os"
	"encoding/json"
	"log"
	"io"
	"fmt"
	"time"
	"strings"
	"strconv"
	"io/ioutil"
	"github.com/revel/config"
	"github.com/jlaffaye/ftp"
)

type TemplateItem struct {
	Code string
	Label string
	Value string
}

type App struct {
	*revel.Controller
}

func (c App) Index() revel.Result {

	file, _ := os.Open("conf/config.json")

	fmt.Println(file)
	templatesData := []TemplateItem{}
	decoder := json.NewDecoder(file)
	for {
		templateItem := new(TemplateItem)
		if err := decoder.Decode(&templateItem); err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}

		templatesData = append(templatesData, *templateItem)
	}
	Content := templatesData
	HeadScripts := []string{
		"js/base.js",
		"js/bootbox.js",
		"js/bootstrap.js",
	}
	return c.Render(Content, HeadScripts)
}

func (c App) GenerateCommand() revel.Result {
	currentTime := int32(time.Now().Unix())

	requestItems := c.Params.Form
	data := make(map[string]string);
	for key, value := range requestItems {
		data[key] = strings.Join(value, "")
	}
	jsonString, _ :=json.Marshal(data)
	jsonData := []byte(string(jsonString))
	jsonFilename := strings.Join([]string{"report_", strconv.Itoa(int(currentTime)), ".json"}, "")
	ioutil.WriteFile(strings.Join([]string{"data/temp/", jsonFilename}, ""), jsonData, 0644)

	ftpconf, _ := config.ReadDefault("conf/ftp.conf")

	ftpHost, _ := ftpconf.String("DEFAULT", "FtpHost")
	ftpPort, _ := ftpconf.String("DEFAULT", "FtpPort")
	ftpUrl := strings.Join([]string{ftpHost, ":", ftpPort}, "")
	ftp, _ := ftp.Connect(ftpUrl)

	ftpUser, _ := ftpconf.String("DEFAULT", "FtpUser")
	ftpPass, _ := ftpconf.String("DEFAULT", "FtpPass")
	ftp.Login(ftpUser, ftpPass)

	ftpDataUploadDir, _ := ftpconf.String("DEFAULT", "FtpDataUploadDir")
	jsonReader, _ := os.Open(strings.Join([]string{"data/temp/", jsonFilename}, ""))
	ftp.Stor(strings.Join([]string{ftpDataUploadDir, "/", jsonFilename}, ""), jsonReader)

	ftpTemplaUploadDir, _ := ftpconf.String("DEFAULT", "FtpTemplateUploadDir")
	templateReader, _ := os.Open("data/template.docx")
	ftp.Stor(strings.Join([]string{ftpTemplaUploadDir, "/", "report_", strconv.Itoa(int(currentTime)), ".docx"}, ""), templateReader)

	ftp.Quit()

	result := make(map[string]string, 2)
	result["success"] = "1"
	result["name"] = strconv.Itoa(int(currentTime))
	return c.RenderJson(result)
}

func (c App) CheckCommand() revel.Result {
	fileName := c.Params.Form.Get("name")
	fullFileName := strings.Join([]string{"report_", fileName, ".docx"}, "")

	ftpconf, _ := config.ReadDefault("conf/ftp.conf")

	ftpHost, _ := ftpconf.String("DEFAULT", "FtpHost")
	ftpPort, _ := ftpconf.String("DEFAULT", "FtpPort")
	ftpUrl := strings.Join([]string{ftpHost, ":", ftpPort}, "")
	ftp, _ := ftp.Connect(ftpUrl)
	ftpUser, _ := ftpconf.String("DEFAULT", "FtpUser")
	ftpPass, _ := ftpconf.String("DEFAULT", "FtpPass")
	ftp.Login(ftpUser, ftpPass)

	ftpDownloadDir, _ := ftpconf.String("DEFAULT", "FtpDownloadDir")
	files, _ := ftp.NameList(ftpDownloadDir)

	result := make(map[string]string, 1)
	for i := range files {
		if strings.Contains(files[i], fullFileName) {
			result["success"] = "1"
			ftp.Quit()
			return c.RenderJson(result)
		}
	}


	ftpErrorDir, _ := ftpconf.String("DEFAULT", "FtpErrorDir")
	files, _ = ftp.NameList(ftpErrorDir)
	for i := range files {
		if strings.Contains(files[i], fullFileName) {
			result["success"] = "-1"
			ftp.Quit()
			return c.RenderJson(result)
		}
	}

	result["success"] = "0"
	ftp.Quit()
	return c.RenderJson(result)
}

func (c App) GetCommand() revel.Result {

	fileName := c.Params.Query.Get("name")
	fmt.Println(fileName)
	fullFileName := strings.Join([]string{"report_", fileName, ".docx"}, "")

	ftpconf, _ := config.ReadDefault("conf/ftp.conf")

	ftpHost, _ := ftpconf.String("DEFAULT", "FtpHost")
	ftpPort, _ := ftpconf.String("DEFAULT", "FtpPort")
	ftpUrl := strings.Join([]string{ftpHost, ":", ftpPort}, "")
	ftp, _ := ftp.Connect(ftpUrl)
	ftpUser, _ := ftpconf.String("DEFAULT", "FtpUser")
	ftpPass, _ := ftpconf.String("DEFAULT", "FtpPass")
	ftp.Login(ftpUser, ftpPass)

	ftpDownloadDir, _ := ftpconf.String("DEFAULT", "FtpDownloadDir")
	ftpFileName := strings.Join([]string{ftpDownloadDir, "/", fullFileName}, "")
	fmt.Println(ftpFileName)
	r, _ := ftp.Retr(ftpFileName);
	fileData, _ := ioutil.ReadAll(r);

	resultFilename := "data/result/" + fullFileName
	ioutil.WriteFile(resultFilename, fileData, 777);


	c.Response.Out.Header().Add("Content-Type", "application/download")
	c.Response.Out.Header().Add("Content-Disposition", strings.Join([]string{"attachment; filename=", fullFileName}, ""))
	c.Response.Out.Write(fileData)
	return c.Render()
}
